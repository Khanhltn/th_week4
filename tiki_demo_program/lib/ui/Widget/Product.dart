import 'package:flutter/material.dart';


class Product {
  final String image, title;
  final int price;
  final Color bgColor;

  Product({
    required this.image,
    required this.title,
    required this.price,
    this.bgColor = const Color(0xFFEFEFF2),
  });
}

List<Product> demo_product2 = [
  Product(
    image: "assets/mobile_top.png",
    title: "VIVO V11",
    price: 100,
    bgColor: const Color(0xFFFEFBF9),
  ),
  Product(
    image: "assets/mobiles.png",
    title: "Smart Phone",
    price: 80,
  ),
  Product(
    image: 'assets/nokia.png',
    title: "Smart phone Nokia",
    price: 90,
    bgColor: const Color(0xFFF8FEFB),
  ),
  Product(
    image: "assets/phone.png",
    title: "Smart phone super Ultra",
    price: 149,
    bgColor: const Color(0xFFEEEEED),
  ),
  Product(
    image: "assets/phone_three.png",
    title: "Iphone 13 pro max",
    price: 149,
    bgColor: const Color(0xFFEEEEED),
  ),


];

List<Product> demo_product = [
  Product(
    image: "assets/running.png",
    title: "Machines running",
    price: 300,
    bgColor: const Color(0xFFFEFBF9),
  ),
  Product(
    image: "assets/product_1.png",
    title: "Sport shoes",
    price: 60,
  ),
  Product(
    image: 'assets/product_0.png',
    title: "Leather shoes",
    price: 200,
    bgColor: const Color(0xFFF8FEFB),
  ),
  Product(
    image: "assets/telivision.png",
    title: "Mi TV",
    price: 149,
    bgColor: const Color(0xFFEEEEED),
  ),
  Product(
    image: "assets/laptop.png",
    title: "Latop Acer core i5",
    price: 149,
    bgColor: const Color(0xFFEEEEED),
  ),


];