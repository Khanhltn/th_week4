import 'package:flutter/material.dart';
import 'package:tiki_demo_program/ui/Widget/Deals2.dart';
import 'package:tiki_demo_program/ui/Widget/SmartPhone.dart';
import 'package:tiki_demo_program/ui/Widget/Top_product.dart';
import 'Deals.dart';

class body extends StatefulWidget {
  @override
  _bodyState createState() => _bodyState();
}

class _bodyState extends State<body>{
  @override
  Widget build(BuildContext context) {
    return ListView(
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(10),
            child: Title(color: Colors.black, child: Text('Best seller')),
          ),
          SizedBox(height: 10,width: 10,),
          Firstlist(),
          Container(
            margin: EdgeInsets.all(10),
            child: Title(color: Colors.black, child: Text('Hot deal in day')),
          ),
          Fourthlist(),
          Container(
            margin: EdgeInsets.all(10),
            child: Title(color: Colors.black, child: Text('New Arivials')),
          ),
          Thirdlist(),
          Secondlist(),
        ]
    );
  }
}