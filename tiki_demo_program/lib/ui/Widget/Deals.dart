import 'package:flutter/material.dart';

class Secondlist extends StatefulWidget {
  @override
  _SecondlistState createState() => _SecondlistState();
}

class _SecondlistState extends State<Secondlist> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      height: MediaQuery.of(context).size.height / 2.5,
      child: Card(
        child: Row(
          children: <Widget>[
            Expanded(
              child: Image.asset(
                  "assets/laptop.png"),
            ),
            Expanded(
              child: Image.asset(
                  "assets/hair_dryer.png"),
            )
          ],
        ),
      ),
    );
  }
}
