import 'package:flutter/material.dart';

class Fourthlist extends StatefulWidget {
  @override
  _FourthlistState createState() => _FourthlistState();
}

class _FourthlistState extends State<Fourthlist> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      height: MediaQuery.of(context).size.height / 2.5,
      child: Card(
        child: Row(
          children: <Widget>[
            Expanded(
              child: Image.asset(
                  "assets/power_bank.png"),
            ),
            Expanded(
              child: Image.asset(
                  "assets/banner_one.png"),
            )
          ],
        ),
      ),
    );
  }
}
