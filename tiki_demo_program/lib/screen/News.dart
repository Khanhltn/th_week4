import 'package:flutter/material.dart';

class News extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return newsBody();
  }

}
class newsBody extends State<StatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ListView(
          children: [
            Text("Sản phẩm đọc đáo được bày bắn trên thị trường hiện nay", style: TextStyle(fontSize: 30, wordSpacing: 10),),
            Image.asset("assets/product_1.png"),
            Text("Hạy cùng Bitis tạo nên thương hiệu giày đẳng cấp Việt Nam"),
            Image.asset("assets/product_3.png"),
            Image.asset("assets/watch.png"),
            Image.asset("assets/product_2.png"),
          ],
        ),
      ),
    );
  }
}