import 'package:flutter/material.dart';
import '../mixins/mixin_common_validation.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() => LoginState();

}
/* Class LoginState kế thừa statefulwidget để chứa các widget như Thanh nhập email, password, button*/
class LoginState extends State<StatefulWidget> with commonValidation{
  final formKey = GlobalKey<FormState>();
  late String email;
  late String password;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(10),
        child: Form(
          key: formKey,
          child: Column(
            children: [
              emailField(),
              passwordField(),
              Container(margin: EdgeInsets.only(top:10),),
              loginbutton(),
            ],
          ),
        )
    );
  }

  /* Thanh nhập Email adrress */
  Widget emailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Email address'
      ),
      validator: validateEmail,
      //onSaved lưu dữ liệu đã được nhập
      onSaved: (value){
        // Print để hiển thị value đã nhập
        email = value as String;
      },
    );
  }

  /* Thanh nhập password */
  Widget passwordField() {
    return TextFormField(
      //obscureText dùng để ẩn đi văn bản khi nhập
      obscureText: true,
      decoration: InputDecoration(
          icon: Icon(Icons.password),
          labelText: 'Password'
      ),
      validator: validatePassword,
      //onSaved lưu dữ liệu đã được nhập
      onSaved: (value){
        // Print để hiển thị value đã nhập
        password = value as String;
      },
    );
  }

  /* Nút Login */
  Widget loginbutton() {
    return ElevatedButton(
      /* Tạo sự kiện onPressed  khi nhấn button */
      onPressed: () {
        if(formKey.currentState!.validate()){
          formKey.currentState!.save();

        }
      },
      child: Text('GO'),
    );
  }
}